#!/bin/python3

import re
import sys
import requests
import pandas as pd
from random import randint
from datetime import datetime
from sqlalchemy import create_engine
 
def formaturl(url):
  if not re.match('(?:http|ftp|https)://', url):
      return 'https://www.{}'.format(url)
  return url

def main():
  BASE_URL = 'https://stage.api.pricebook.co.id/'
  SHOPPRODUCT_API_URL = BASE_URL + 'api/shop_product'
  SHOP_API_URL = BASE_URL + 'api/shop'


  # read csv file and save to df_pricematch_csv
  df_pricematch_csv = pd.read_csv('./'+sys.argv[1], header = 0, delimiter=";", encoding = "ISO-8859-1", keep_default_na=False, na_values=[""])

  # separate df_pricematch_csv by shop_id and shop_id_crawler
  # when df_pricematch_csv only have shop_id it mean they already exist in shop table->(database pricebook)
  # when df_pricematch_csv only have shop_id_crawler it mean they not exist in shop table->(database pricebook)
  df_pricematch_exist_shop = df_pricematch_csv[(df_pricematch_csv['shop_id_crawler'].isnull())]
  df_pricematch_new_shop = df_pricematch_csv[(df_pricematch_csv['shop_id'].isnull())]


  # 1. Processing df_pricematch_csv already exist in shop table->(database pricebook) to shop_product table->(database pricebook)
  # before inserting, check is data already exist in shop_product->(database pricebook)
  # only insert df_pricematch_exist_shop that not exist in shop_product->(database pricebook)

  # create dataframe df_shop_product_pb from shop_product table->(database pricebook)
  # merge df_shop_product_pb and df_pricematch_exist_shop
  # get only shop_product that not exist in df_shop_product_pb
  pricebookdb_connection = 'mysql://root:root@127.0.0.1/pricebook'
  df_shop_product_pb = pd.read_sql('select * from shop_product where enabled_f = 1', con=pricebookdb_connection)
  df_merge_shopproduct = pd.merge(left=df_shop_product_pb, 
                              right=df_pricematch_exist_shop, 
                              on='url_md5', how='right', 
                              suffixes=('_pricebookdb', '_crawler'))
  df_merge_shopproduct_outerjoin = df_merge_shopproduct[df_merge_shopproduct['product_name'].isnull()]

  # insert df_merge_shopproduct_outerjoin to shop_product->(database pricebook)
  for row in df_merge_shopproduct_outerjoin.itertuples():
    shopproduct_data = {
      "product_id" : row.product_id_crawler,
      "product_name" : row.product_name_crawler,
      "price" : row.price_crawler,
      "color" : "",
      "note": "",
      "url": row.url_crawler,
      "platform_id": row.platform_id_crawler
    }
    shop_product_result = requests.post(url = SHOPPRODUCT_API_URL, data = shopproduct_data)
    shop_product_post_result = shop_product_result.json()


  # 2. Processing df_pricematch_csv not exist in shop table->(database pricebook) to shop_product table->(database pricebook)
  # read shop table->(database crawler)
  # and merge with pricematch_new_shop using shop_id column to get all shop data
  crawlerdb_connection = 'mysql://root:root@127.0.0.1/crawler'
  df_shop_crawler = pd.read_sql('select * from shop where parent_shop_id != 0', con=crawlerdb_connection)
  df_merged_newshop_and_shopproduct = pd.merge(left=pricematch_new_shop,
                                          right=df_shop_crawler,
                                          left_on='shop_id_crawler', 
                                          right_on='shop_id', 
                                          suffixes=('_df', '_crawlerdb'))


  all_shop_id = []
  for row in df_merged_newshop_and_shopproduct.itertuples():
    shop_data = {
      "shop_url" : shop_url = formaturl(row.url_crawlerdb),
      "shop_name": row.shop_name,
      "city": row.city,
      "platform": "tokopedia"
    }
    shop_post_result = requests.post(url = SHOPPRODUCT_API_URL, data = shopproduct_data)
    shop_id = {
      "url_md5": df_merged_newshop_and_shopproduct["url_md5"],
      "shop_id_new": shop_post_result["data"]["shop_id"]
    }
    all_shop_id.append(shop_id)

    new_shop_id = pd.dataframe(all_shop_id)
    
    df_shop_product_new_id = pd.merge(left=df_pricematch_new_shop, 
                                  right=df_new_shop_id, 
                                  left_on='url_md5', 
                                  right_on='url_md5')

    for row in df_shop_product_new_id.itertuples():
      shopproduct_data = {
      "product_id" : row.product_id,
      "product_name" : row.product_name,
      "price" : row.price,
      "color" : "",
      "note": "",
      "url": row.url,
      "platform_id": row.platform_id
    }
    shop_product_result = requests.post(url = SHOPPRODUCT_API_URL, data = shopproduct_data)
    shop_product_post_result = shop_product_result.json()

if __name__ == "__main__":
  main()
